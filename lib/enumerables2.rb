require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|el| el.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  arr = string.split("") - [" "]
  arr = arr.select {|el| arr.count(el) > 1}
  arr.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  arr = string.delete(",.!?';:-").split(" ")
  most_long = long_word(arr)
  arr = arr - [most_long]
  second_long = long_word(arr)
  [most_long, second_long]
end

def long_word(arr)
  arr.reduce("") do |longest, el|
    if el.length > longest.length
      el
    else
      longest
    end
  end
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ('a'..'z').to_a
  alphabet.reject { |let| string.include?(let) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  split_yr = year.to_s.split("")
  split_yr.uniq == split_yr
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  # takes a song list and only returns songs without repeats
  songs.select{ |elem| no_repeats?(elem, songs) }.uniq
end

def no_repeats?(song_name, songs)
  # looking for songs that repeat
  # song repeats => false
  songs.each_with_index do |elem, ind|
    # target songs appears twice
    if elem == song_name && songs[ind + 1] == song_name
      return false
    # stops last instance of a repeating song from looking like a OWW
  elsif elem == song_name && songs[ind - 1] == song_name
      return false
    end
  end
  # if the given song does not repeat (no conditions triggered), it is a OWW
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  arr = string.split(" ")
  arr.reduce("") do |acc, el|
    remove_punctuation(el)
    if !el.include?('c')
      acc
    elsif acc == ""
      el
    elsif c_distance(acc) > c_distance(el)
      el
    else
      acc
    end
  end
end

def c_distance(word)
  counter = 0
  word.reverse.each_char do |char|
    if char == 'c'
      return counter
    else
      counter += 1
    end
  end
end

def remove_punctuation(word)
  word.delete!(".,?!;:'-")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  nested_arrs = []
  first_ind = nil
  arr.each_with_index do |elem, ind|
    # skip single numbers
    if arr.count(elem) == 1
      next
    # adds if number repeats
    elsif elem == arr[ind+1]
      first_ind ||= ind
    # elem != arr[ind+1]
    else
      nested_arrs.push([first_ind, ind])
      first_ind = nil
    end
  end
  nested_arrs
end
